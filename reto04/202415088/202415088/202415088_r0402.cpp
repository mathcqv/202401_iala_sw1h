#include <iostream>
#include <iomanip>
#include <random>

using namespace std;

int **matriz(int *n, int *m) {
	int** matriz = new int* [*n];
	for (int i = 0; i < *n; ++i)	{
		matriz[i] = new int[*m];
		for (int j = 0; j < *m; ++j) {
			matriz[i][j] = 0;
		}
	}
	return matriz;
}

void printMatriz(int **matriz, int *n, int *m) {
	for (int i = 0; i < *n; ++i)	{
		for (int j = 0; j < *m; ++j)	{
			cout << " " << matriz[i][j];
		}
		cout << endl;
	}
}

void deleteMatriz(int** matriz, int *n) {
	for (int i = 0; i < *n; i++)	{
		delete[]matriz[i];
	}
	delete[]matriz;
}

int main() {
	int* n = new int;
	int* m = new int;
	int** matrix;

	cout << "Ingrese el n�mero de filas de la matriz: ";
	cin >> *n;
	cout << "Ingrese el n�mero de columnas de la matriz: ";
	cin >> *m;
	matrix = matriz(n, m);

	printMatriz(matrix, n, m);
		
	deleteMatriz(matrix, n);
	delete n;
	delete m;

	system("pause");
	return EXIT_SUCCESS;
}