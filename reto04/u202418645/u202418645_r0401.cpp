// problema resuelto
#include "iostream"

using namespace std;

inline int randint(int a, int b) { return rand() % (b - a) + a; }

void randomarray() {

	int arraycapacity = randint(100, 501);

	int* array = new int[arraycapacity];

	for (int i = 0; i < arraycapacity; ++i) {
		int fillarray = randint(1, 1001);
		array[i] = fillarray;
	}

	return;
}